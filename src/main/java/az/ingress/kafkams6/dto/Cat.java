package az.ingress.kafkams6.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cat {

    private String name;

    private String lastName;

    private String catColor;

    private Integer age;
}
