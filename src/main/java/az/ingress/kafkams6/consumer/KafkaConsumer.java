package az.ingress.kafkams6.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    @KafkaListener(topics = "my-first-kafka-topic", groupId = "foo2")
    public void listenGroupFoo(Object message) {
        System.out.println("Received Message in group foo: " + message.toString());
    }
}
